mod disassembly;

use std::{fs, path::PathBuf};

use clap::Parser;

use crate::disassembly::Disassembler;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    file: PathBuf,
}

fn main() {
    let args = Args::parse();

    let file_contents = fs::read(args.file).unwrap();

    let disassembler = Disassembler::new(&file_contents);

    let instructions: Vec<_> = disassembler.collect();

    println!("{:?}", instructions);
    println!();
    println!("Disassembled {} instructions", instructions.len());
}
