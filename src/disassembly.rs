#![allow(clippy::bad_bit_mask)]

use bitmatch::bitmatch;
use strum::{EnumIter, IntoEnumIterator};

trait FromBitPattern: IntoEnumIterator {
    const BIT_MASK: u8;

    fn bit_pattern(&self) -> u8;

    fn from_bit_pattern(byte: u8, shift_offset: usize) -> Option<Self> {
        let shifted_byte = (byte >> shift_offset) & Self::BIT_MASK;

        Self::iter().find(|reg| shifted_byte ^ reg.bit_pattern() == 0)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, EnumIter)]
pub enum Register {
    A,
    B,
    C,
    D,
    E,
    H,
    L,
    // M for memory, i.e. not actually a register
    M,
}

impl Default for Register {
    fn default() -> Self {
        Self::A
    }
}

impl FromBitPattern for Register {
    const BIT_MASK: u8 = 0b111;

    fn bit_pattern(&self) -> u8 {
        match self {
            Self::A => 0b111,
            Self::B => 0b000,
            Self::C => 0b001,
            Self::D => 0b010,
            Self::E => 0b011,
            Self::H => 0b100,
            Self::L => 0b101,
            Self::M => 0b110,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, EnumIter)]
pub enum RegisterPair {
    BC,
    DE,
    HL,
    SP,
}

impl Default for RegisterPair {
    fn default() -> Self {
        Self::BC
    }
}

impl FromBitPattern for RegisterPair {
    const BIT_MASK: u8 = 0b11;

    fn bit_pattern(&self) -> u8 {
        match self {
            Self::BC => 0b00,
            Self::DE => 0b01,
            Self::HL => 0b10,
            Self::SP => 0b11,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, EnumIter)]
pub enum BranchCondition {
    NZ,
    Z,
    NC,
    C,
    PO,
    PE,
    P,
    M,
}

impl FromBitPattern for BranchCondition {
    const BIT_MASK: u8 = 0b111;

    fn bit_pattern(&self) -> u8 {
        match self {
            Self::NZ => 0b000,
            Self::Z => 0b001,
            Self::NC => 0b010,
            Self::C => 0b011,
            Self::PO => 0b100,
            Self::PE => 0b101,
            Self::P => 0b110,
            Self::M => 0b111,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Instruction {
    Nop,
    // Data Transfer Group
    Mov(Register, Register),
    Mvi(Register, u8),
    Lxi(RegisterPair, u16),
    Lda(u16),
    Sta(u16),
    Lhld(u16),
    Shld(u16),
    Ldax(RegisterPair),
    Stax(RegisterPair),
    Xchg,

    // Arithmetic Group
    Add(Register),
    Adi(u8),
    Adc(Register),
    Aci(u8),
    Sub(Register),
    Sui(u8),
    Sbb(Register),
    Sbi(u8),
    Inr(Register),
    Dcr(Register),
    Inx(RegisterPair),
    Dcx(RegisterPair),
    Dad(RegisterPair),
    Daa,
    // Logical Group
    Ana(Register),
    Ani(u8),
    Xra(Register),
    Xri(u8),
    Ora(Register),
    Ori(u8),
    Cmp(Register),
    Cpi(u8),
    Rlc,
    Rrc,
    Ral,
    Rar,
    Cma,
    Cmc,
    Stc,
    // Branch Group
    Jmp(u16),
    Jcondition(BranchCondition, u16),
    Call(u16),
    Ccondition(BranchCondition, u16),
    Ret,
    Rcondition(BranchCondition),
    Rst(u8),
    Pchl,
    // Stack, I/O and Machine Control Group
    Push(RegisterPair),
    PushPsw,
    Pop(RegisterPair),
    PopPsw,
    Xthl,
    Sphl,
    In(u8),
    Out(u8),
    Ei,
    Di,
    Hlt,
}

impl Instruction {
    fn size(&self) -> usize {
        match self {
            Instruction::Lxi(_, _)
            | Instruction::Lda(_)
            | Instruction::Sta(_)
            | Instruction::Lhld(_)
            | Instruction::Jmp(_)
            | Instruction::Jcondition(_, _)
            | Instruction::Call(_)
            | Instruction::Ccondition(_, _)
            | Instruction::Shld(_) => 3,
            Instruction::Mvi(_, _)
            | Instruction::Adi(_)
            | Instruction::Aci(_)
            | Instruction::Sui(_)
            | Instruction::Ani(_)
            | Instruction::Xri(_)
            | Instruction::Ori(_)
            | Instruction::Cpi(_)
            | Instruction::In(_)
            | Instruction::Out(_)
            | Instruction::Sbi(_) => 2,
            _ => 1,
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Disassembler<'a> {
    input: &'a [u8],
}

impl<'a> Disassembler<'a> {
    pub fn new(input: &'a [u8]) -> Self {
        Self { input }
    }

    fn read_word(&self, offset: usize) -> u16 {
        u16::from_le_bytes([self.input[offset], self.input[offset + 1]])
    }
}

impl Iterator for Disassembler<'_> {
    type Item = Instruction;

    #[bitmatch]
    fn next(&mut self) -> Option<Self::Item> {
        if self.input.is_empty() {
            return None;
        }

        let input = self.input;
        let opcode = input[0];

        let instruction = #[bitmatch]
        match opcode {
            "00000000" => Some(Instruction::Nop),
            "01110110" => Some(Instruction::Hlt),

            // Data Transfer Group
            "01dddsss" => Some(Instruction::Mov(
                Register::from_bit_pattern(d, 0)?,
                Register::from_bit_pattern(s, 0)?,
            )),
            "00ddd110" => Some(Instruction::Mvi(
                Register::from_bit_pattern(d, 0)?,
                input[1],
            )),
            "00pp0001" => Some(Instruction::Lxi(
                RegisterPair::from_bit_pattern(p, 0)?,
                self.read_word(1),
            )),
            "00111010" => Some(Instruction::Lda(self.read_word(1))),
            "00110010" => Some(Instruction::Sta(self.read_word(1))),
            "00101010" => Some(Instruction::Lhld(self.read_word(1))),
            "00100010" => Some(Instruction::Shld(self.read_word(1))),
            "00pp1010" => {
                let pair = RegisterPair::from_bit_pattern(p, 0)?;

                if !(pair == RegisterPair::BC || pair == RegisterPair::DE) {
                    panic!("only register pairs BC or DE may be specified for LDAX");
                }

                Some(Instruction::Ldax(pair))
            }
            "00pp0010" => {
                let pair = RegisterPair::from_bit_pattern(p, 0)?;

                if !(pair == RegisterPair::BC || pair == RegisterPair::DE) {
                    panic!("only register pairs BC or DE may be specified for STAX");
                }

                Some(Instruction::Stax(pair))
            }
            "11101011" => Some(Instruction::Xchg),

            // Arithmetic Group
            "10000sss" => Some(Instruction::Add(Register::from_bit_pattern(s, 0)?)),
            "11000110" => Some(Instruction::Adi(input[1])),
            "10001sss" => Some(Instruction::Adc(Register::from_bit_pattern(s, 0)?)),
            "11001110" => Some(Instruction::Aci(input[1])),
            "10010sss" => Some(Instruction::Sub(Register::from_bit_pattern(s, 0)?)),
            "11010110" => Some(Instruction::Sui(input[1])),
            "10011sss" => Some(Instruction::Sbb(Register::from_bit_pattern(s, 0)?)),
            "11011110" => Some(Instruction::Sbi(input[1])),
            "00ddd100" => Some(Instruction::Inr(Register::from_bit_pattern(d, 0)?)),
            "00ddd101" => Some(Instruction::Dcr(Register::from_bit_pattern(d, 0)?)),
            "00pp0011" => Some(Instruction::Inx(RegisterPair::from_bit_pattern(p, 0)?)),
            "00pp1011" => Some(Instruction::Dcx(RegisterPair::from_bit_pattern(p, 0)?)),
            "00pp1001" => Some(Instruction::Dad(RegisterPair::from_bit_pattern(p, 0)?)),
            "00100111" => Some(Instruction::Daa),

            // Logical Group
            "10100sss" => Some(Instruction::Ana(Register::from_bit_pattern(s, 0)?)),
            "11100110" => Some(Instruction::Ani(input[1])),
            "10101sss" => Some(Instruction::Xra(Register::from_bit_pattern(s, 0)?)),
            "11101110" => Some(Instruction::Xri(input[1])),
            "10110sss" => Some(Instruction::Ora(Register::from_bit_pattern(s, 0)?)),
            "11110110" => Some(Instruction::Ori(input[1])),
            "10111sss" => Some(Instruction::Cmp(Register::from_bit_pattern(s, 0)?)),
            "11111110" => Some(Instruction::Cpi(input[1])),
            "00000111" => Some(Instruction::Rlc),
            "00001111" => Some(Instruction::Rrc),
            "00010111" => Some(Instruction::Ral),
            "00011111" => Some(Instruction::Rar),
            "00101111" => Some(Instruction::Cma),
            "00111111" => Some(Instruction::Cmc),
            "00110111" => Some(Instruction::Stc),

            // Branch Group
            "11000011" => Some(Instruction::Jmp(self.read_word(1))),
            "11ccc010" => Some(Instruction::Jcondition(
                BranchCondition::from_bit_pattern(c, 0)?,
                self.read_word(1),
            )),
            "11001101" => Some(Instruction::Call(self.read_word(1))),
            "11ccc100" => Some(Instruction::Ccondition(
                BranchCondition::from_bit_pattern(c, 0)?,
                self.read_word(1),
            )),
            "11001001" => Some(Instruction::Ret),
            "11ccc000" => Some(Instruction::Rcondition(BranchCondition::from_bit_pattern(
                c, 0,
            )?)),
            "11nnn111" => Some(Instruction::Rst(n)),
            "11101001" => Some(Instruction::Pchl),

            // Stack, I/O and Machine Control Group
            "11110101" => Some(Instruction::PushPsw),
            "11pp0101" => Some(Instruction::Push(RegisterPair::from_bit_pattern(p, 0)?)),
            "11110001" => Some(Instruction::PopPsw),
            "11pp0001" => Some(Instruction::Pop(RegisterPair::from_bit_pattern(p, 0)?)),
            "11100011" => Some(Instruction::Xthl),
            "11111001" => Some(Instruction::Sphl),
            "11011011" => Some(Instruction::In(input[1])),
            "11010011" => Some(Instruction::Out(input[1])),
            "11111011" => Some(Instruction::Ei),
            "11110011" => Some(Instruction::Di),

            _ => panic!("invalid opcode {:x}", opcode),
        };

        if let Some(ref instruction) = instruction {
            self.input = &self.input[instruction.size()..];
        }

        instruction
    }
}

#[cfg(test)]
mod tests {
    use itertools::Itertools;
    use paste::paste;

    use super::*;

    macro_rules! test_simple {
        ($mnemonic:ident, $opcode:literal) => {
            paste! {
                #[test]
                fn [<test_disassemble_ $mnemonic:lower>]() {
                    let input = [$opcode];

                    disassemble_test_helper(&input, vec![Instruction::$mnemonic]);
                }
            }
        };
    }

    macro_rules! test_with_register {
        ($mnemonic:ident, $opcode:literal, $shift:literal) => {
            paste! {
                #[test]
                fn [<test_disassemble_ $mnemonic:lower>]() {
                    for reg in Register::iter() {
                        let opcode = add_bits_to_opcode($opcode, reg, $shift);
                        let input = [opcode];

                        disassemble_test_helper(&input, vec![Instruction::$mnemonic(reg)]);
                    }
                }
            }
        };
    }

    macro_rules! test_with_register_pair {
        ($mnemonic:ident, $opcode:literal, $shift:literal) => {
            paste! {
                #[test]
                fn [<test_disassemble_ $mnemonic:lower>]() {
                    for reg in RegisterPair::iter() {
                        let opcode = add_bits_to_opcode($opcode, reg, $shift);
                        let input = [opcode];

                        disassemble_test_helper(&input, vec![Instruction::$mnemonic(reg)]);
                    }
                }
            }
        };
    }

    macro_rules! test_with_data {
        ($mnemonic:ident, $opcode:literal) => {
            paste! {
                #[test]
                fn [<test_disassemble_ $mnemonic:lower>]() {
                    let data = 0x42;
                    let input = [$opcode, data];

                    disassemble_test_helper(&input, vec![Instruction::$mnemonic(data)]);
                }
            }
        };
    }

    macro_rules! test_with_address {
        ($mnemonic:ident, $opcode:literal) => {
            paste! {
                #[test]
                fn [<test_disassemble_ $mnemonic:lower>]() {
                    let input_data = [0b11110000, 0b00001111];
                    let expected_address = u16::from_le_bytes([input_data[0], input_data[1]]);

                    let input = [$opcode, input_data[0], input_data[1]];

                    disassemble_test_helper(&input, vec![Instruction::$mnemonic(expected_address)]);
                }
            }
        };
    }

    fn disassemble_test_helper(input: &[u8], expected_instructions: Vec<Instruction>) {
        let disassembler = Disassembler::new(input);
        assert_eq!(disassembler.collect::<Vec<_>>(), expected_instructions);
    }

    fn add_bits_to_opcode<R>(base_opcode: u8, r: R, shift: usize) -> u8
    where
        R: FromBitPattern,
    {
        base_opcode | (r.bit_pattern() << shift)
    }

    #[test]
    fn test_register_from_bitmask() {
        assert_eq!(
            Register::from_bit_pattern(0b00_111_000, 3).unwrap(),
            Register::A
        );
        assert_eq!(
            Register::from_bit_pattern(0b00_000_000, 3).unwrap(),
            Register::B
        );
        assert_eq!(
            Register::from_bit_pattern(0b00_001_000, 3).unwrap(),
            Register::C
        );
        assert_eq!(
            Register::from_bit_pattern(0b00_010_000, 3).unwrap(),
            Register::D
        );
        assert_eq!(
            Register::from_bit_pattern(0b00_011_000, 3).unwrap(),
            Register::E
        );
        assert_eq!(
            Register::from_bit_pattern(0b00_100_000, 3).unwrap(),
            Register::H
        );
        assert_eq!(
            Register::from_bit_pattern(0b00_101_000, 3).unwrap(),
            Register::L
        );
        assert_eq!(
            Register::from_bit_pattern(0b00_110_000, 3).unwrap(),
            Register::M
        );
    }

    #[test]
    fn test_register_pair_from_bitmask() {
        assert_eq!(
            RegisterPair::from_bit_pattern(0b00000000, 3).unwrap(),
            RegisterPair::BC
        );
        assert_eq!(
            RegisterPair::from_bit_pattern(0b00001000, 3).unwrap(),
            RegisterPair::DE
        );
        assert_eq!(
            RegisterPair::from_bit_pattern(0b00010000, 3).unwrap(),
            RegisterPair::HL
        );
        assert_eq!(
            RegisterPair::from_bit_pattern(0b00011000, 3).unwrap(),
            RegisterPair::SP
        );
    }

    #[test]
    fn test_branch_condition_from_bitmask() {
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00000000, 3).unwrap(),
            BranchCondition::NZ
        );
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00001000, 3).unwrap(),
            BranchCondition::Z
        );
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00010000, 3).unwrap(),
            BranchCondition::NC
        );
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00011000, 3).unwrap(),
            BranchCondition::C
        );
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00100000, 3).unwrap(),
            BranchCondition::PO
        );
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00101000, 3).unwrap(),
            BranchCondition::PE
        );
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00110000, 3).unwrap(),
            BranchCondition::P
        );
        assert_eq!(
            BranchCondition::from_bit_pattern(0b00111000, 3).unwrap(),
            BranchCondition::M
        );
    }

    test_simple!(Nop, 0b00000000);

    #[test]
    fn test_disassemble_multi_nop() {
        let input = [0x0, 0x0, 0x0];

        disassemble_test_helper(
            &input,
            vec![Instruction::Nop, Instruction::Nop, Instruction::Nop],
        )
    }

    #[test]
    fn test_disassemble_move() {
        let base_opcode = 0b01000000;

        for (dest, src) in Register::iter().tuple_combinations() {
            let opcode = add_bits_to_opcode(base_opcode, dest, 3);
            let opcode = add_bits_to_opcode(opcode, src, 0);
            let input = [opcode];

            disassemble_test_helper(&input, vec![Instruction::Mov(dest, src)])
        }
    }

    #[test]
    fn test_disassemble_move_immediate() {
        let base_opcode = 0b00000110;
        let input_data = 0b11110000;

        for reg in Register::iter() {
            let opcode = add_bits_to_opcode(base_opcode, reg, 3);
            let input = [opcode, input_data];

            disassemble_test_helper(&input, vec![Instruction::Mvi(reg, input_data)])
        }
    }

    #[test]
    fn test_disassemble_load_register_pair_immediate() {
        let base_opcode = 0b00000001;
        let input_data = [0b11110000, 0b00001111];
        let expected_address = u16::from_le_bytes([input_data[0], input_data[1]]);

        for reg_pair in RegisterPair::iter() {
            let opcode = add_bits_to_opcode(base_opcode, reg_pair, 4);
            let input = [opcode, input_data[0], input_data[1]];

            disassemble_test_helper(&input, vec![Instruction::Lxi(reg_pair, expected_address)])
        }
    }

    test_with_address!(Lda, 0b00111010);

    test_with_address!(Sta, 0b00110010);

    test_with_address!(Lhld, 0b00101010);

    test_with_address!(Shld, 0b00100010);

    #[test]
    fn test_disassemble_load_accumulator_indirect() {
        let base_opcode = 0b00001010;

        for reg_pair in [RegisterPair::BC, RegisterPair::DE] {
            let opcode = add_bits_to_opcode(base_opcode, reg_pair, 4);
            let input = [opcode];

            disassemble_test_helper(&input, vec![Instruction::Ldax(reg_pair)])
        }
    }

    #[test]
    fn test_disassemble_store_accumulator_indirect() {
        let base_opcode = 0b00000010;

        for reg_pair in [RegisterPair::BC, RegisterPair::DE] {
            let opcode = add_bits_to_opcode(base_opcode, reg_pair, 4);
            let input = [opcode];

            disassemble_test_helper(&input, vec![Instruction::Stax(reg_pair)])
        }
    }

    test_simple!(Xchg, 0b11101011);

    test_with_register!(Add, 0b10000000, 0);

    test_with_data!(Adi, 0b11000110);

    test_with_register!(Adc, 0b10001000, 0);

    test_with_data!(Aci, 0b11001110);

    test_with_register!(Sub, 0b10010000, 0);

    test_with_data!(Sui, 0b11010110);

    test_with_register!(Sbb, 0b10011000, 0);

    test_with_data!(Sbi, 0b11011110);

    test_with_register!(Inr, 0b00000100, 3);

    test_with_register!(Dcr, 0b00000101, 3);

    test_with_register_pair!(Inx, 0b00000011, 4);

    test_with_register_pair!(Dcx, 0b00001011, 4);

    test_with_register_pair!(Dad, 0b00001001, 4);

    test_simple!(Daa, 0b00100111);

    test_with_register!(Ana, 0b10100000, 0);

    test_with_data!(Ani, 0b11100110);

    test_with_register!(Xra, 0b10101000, 0);

    test_with_data!(Xri, 0b11101110);

    test_with_register!(Ora, 0b10110000, 0);

    test_with_data!(Ori, 0b11110110);

    test_with_register!(Cmp, 0b10111000, 0);

    test_with_data!(Cpi, 0b11111110);

    test_simple!(Rlc, 0b00000111);

    test_simple!(Rrc, 0b00001111);

    test_simple!(Ral, 0b00010111);

    test_simple!(Rar, 0b00011111);

    test_simple!(Cma, 0b00101111);

    test_simple!(Cmc, 0b00111111);

    test_simple!(Stc, 0b00110111);

    test_with_address!(Jmp, 0b11000011);

    #[test]
    fn test_disassemble_jcondition() {
        let opcode = 0b11000010;
        let input_data = [0b11110000, 0b00001111];
        let expected_address = u16::from_le_bytes([input_data[0], input_data[1]]);

        for branch_condtion in BranchCondition::iter() {
            let opcode = add_bits_to_opcode(opcode, branch_condtion, 3);
            let input = [opcode, input_data[0], input_data[1]];

            disassemble_test_helper(
                &input,
                vec![Instruction::Jcondition(branch_condtion, expected_address)],
            );
        }
    }

    test_with_address!(Call, 0b11001101);

    #[test]
    fn test_disassemble_ccondition() {
        let opcode = 0b11000100;
        let input_data = [0b11110000, 0b00001111];
        let expected_address = u16::from_le_bytes([input_data[0], input_data[1]]);

        for branch_condtion in BranchCondition::iter() {
            let opcode = add_bits_to_opcode(opcode, branch_condtion, 3);
            let input = [opcode, input_data[0], input_data[1]];

            disassemble_test_helper(
                &input,
                vec![Instruction::Ccondition(branch_condtion, expected_address)],
            );
        }
    }

    test_simple!(Ret, 0b11001001);

    #[test]
    fn test_disassemble_rcondition() {
        let opcode = 0b11000000;

        for branch_condtion in BranchCondition::iter() {
            let opcode = add_bits_to_opcode(opcode, branch_condtion, 3);
            let input = [opcode];

            disassemble_test_helper(&input, vec![Instruction::Rcondition(branch_condtion)]);
        }
    }

    #[test]
    fn test_disassemble_rst() {
        let opcode: u8 = 0b11000111;

        for partial_addr in (0..=0b111)
            .permutations(3)
            .map(|v| v.iter().fold(0, |acc, bit| acc | bit))
            .collect::<Vec<_>>()
        {
            let opcode = opcode | (partial_addr << 3);
            let input = [opcode];

            disassemble_test_helper(&input, vec![Instruction::Rst(partial_addr)]);
        }
    }

    test_simple!(Pchl, 0b11101001);

    #[test]
    fn test_disassemble_push() {
        let base_opcode = 0b11000101;

        for reg_pair in [RegisterPair::BC, RegisterPair::DE, RegisterPair::HL] {
            let opcode = add_bits_to_opcode(base_opcode, reg_pair, 4);
            let input = [opcode];

            disassemble_test_helper(&input, vec![Instruction::Push(reg_pair)])
        }
    }

    test_simple!(PushPsw, 0b11110101);

    #[test]
    fn test_disassemble_pop() {
        let base_opcode = 0b11000001;

        for reg_pair in [RegisterPair::BC, RegisterPair::DE, RegisterPair::HL] {
            let opcode = add_bits_to_opcode(base_opcode, reg_pair, 4);
            let input = [opcode];

            disassemble_test_helper(&input, vec![Instruction::Pop(reg_pair)])
        }
    }

    test_simple!(PopPsw, 0b11110001);

    test_simple!(Xthl, 0b11100011);

    test_simple!(Sphl, 0b11111001);

    test_with_data!(In, 0b11011011);

    test_with_data!(Out, 0b11010011);

    test_simple!(Ei, 0b11111011);

    test_simple!(Di, 0b11110011);

    test_simple!(Hlt, 0b01110110);
}
